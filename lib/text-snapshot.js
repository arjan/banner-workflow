var phantom = require('phantom');
var delay = require('timeout-as-promise');

const JQUERY = 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js';

function textSnapshot(phantomInstance, selector, outfile, basePath, url) {

  var sitepage, dimensions;
  return phantomInstance
    .createPage()
    .then(page => {
      sitepage = page;
      page.property('viewportSize', {width: 300, height: 250});
      page.property('clipRect', {width: 300, height: 250});
      return page.open(url);
    })
    .then(status => {
      console.log(`Rendering screenshot for ${selector}...`);
      return delay(0);
    })
    .then(() => {
      sitepage.includeJs(JQUERY);
      return sitepage.evaluateJavaScript(`function() { var p = $("${selector}"); var o = p.offset(); o.width = p.width(); o.height = p.height(); return o; }`);
    })
    .then(dim => {
      dimensions = dim;
      return phantomInstance.createPage();
    })
    .then(page => {
      sitepage = page;
      sitepage.property('clipRect', dimensions);
      sitepage.property('viewportSize', {width: 300, height: 250});
      return sitepage.open(url);
    })
    .then(status => {
      sitepage.includeJs(JQUERY);
      sitepage.includeJs(`file://${basePath}/lib/inject.js`);
      sitepage.evaluateJavaScript(`(function() { makeTransparent('${selector}'); })`);
      return delay(2000);
    })
    .then(() => {
      return sitepage.render(outfile);
    })
    .catch(error => {
      console.log(error);
      phantomInstance.exit();
    });
}


module.exports = function (basePath, url) {
  return phantom
    .create()
    .then(instance => {
      var creator = {
        create: function(selector, outfile) {
          return textSnapshot(instance, selector, outfile, basePath, url).then(() => creator);
        },
        exit: function() {
          instance.exit();
        }
      };
      return creator;
    });
};
