function makeTransparent(selector) {
  var exceptionElement = $(selector);
  var sheet = "<style type='text/css'>body.opacity, body.opacity * { opacity: 0; background-color: transparent !important; background: none!important; } " + selector + ", " + selector + " * { opacity: 1!important; }</style>";
  $(sheet).appendTo("head");
  $("body").addClass("opacity");
  console.log(exceptionElement);

  exceptionElement.parents().css('opacity', '1');
};
