var gulp = require('gulp');

var textSnapshot = require('./lib/text-snapshot');

gulp.task('default', function(done) {

  var basePath = '/home/arjan/devel/benbanner/screenshottool';
  var url = `file://${basePath}/banners/300x250-test/index.html`;
  
  textSnapshot(basePath, url)
    .then(creator => creator.create("p", "build/p.png"))
    .then(creator => creator.create("div.timer", "build/timer.png"))
    .then(creator => { creator.exit(); done(); });
  
});
